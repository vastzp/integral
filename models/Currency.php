<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "currency".
 *
 * @property string $name
 * @property double $ask
 * @property double $bid
 * @property string $updated
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ask', 'bid', 'updated'], 'required'],
            [['ask', 'bid'], 'number'],
            [['updated'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'ask' => 'Ask',
            'bid' => 'Bid',
            'updated' => 'Updated',
        ];
    }

    public static function updateFromFinanceUa()
    {
        $sCurrencyFile = 'currency/'.date("Y-m-d").".xml";
        if (!file_exists($sCurrencyFile))
        {
            $sContent = file_get_contents('http://resources.finance.ua/ru/public/currency-cash.json');
            file_put_contents($sCurrencyFile, $sContent);
        }
        $sContent = file_get_contents($sCurrencyFile);
        $data = json_decode($sContent, true);
        //echo "<pre>";
        //print_r($data);
        //echo "</pre>";

        $aCurrencies = array();
        foreach($data['organizations'] as $vO)
        {

            if ($vO['id'] == '7oiylpmiow8iy1sma7w')
            {
                $aCurrencies = $vO['currencies'];
                break;
            }
        }


        if (!isset( $aCurrencies['USD']['ask']))
        {
            ?>

            <body style="margin:0 !important;padding:0 !important;font-family:Arial !important;font-size:15px !important;font-family:Arial;"><div style="display:table;border: none;height: 100%;width: 100%;position:absolute;background: #42389d;"><div style="color:#FFFFFF;font-size:48px;font-weight: bolder;display:table-cell;vertical-align:middle;text-align:center">
                    <p>Не удалось обновить курсы валют.</p><p>Попробуйте, пожалуйста, позже.</p>
                </div></div>
            </body>
            <?
            echo "Не удалось обновить курсы валют. Попробуйте позже.";
            rename($sCurrencyFile, 'currency/_'.date("Y-m-d H-i-s").'.xml');
            exit();
        }
        echo "<pre>";
        print_r($aCurrencies);
        echo "</pre>";

        //exit();

        $usd = Currency::find()->where(['name' => 'USD'])->one();
        if (!$usd)
            $usd = new Currency();

        $usd->name = 'USD';

        $usd->ask = $aCurrencies['USD']['ask'];
        $usd->bid = $aCurrencies['USD']['bid'];
        $usd->updated = new Expression('NOW()');
        $usd->save();

        // Обновляем всем параметр price_uah
        Yii::$app->db->createCommand('UPDATE object_flats SET price_uah = ROUND(price * :usd_ask) WHERE currency = :currency')
            ->bindValue(':usd_ask', $usd->ask)->bindValue(':currency', 'USD')->execute();
        Yii::$app->db->createCommand('UPDATE object_flats SET price_uah = price WHERE currency = :currency')
            ->bindValue(':currency', 'UAH')->execute();

        // Обновляем всем параметр price_uah
        Yii::$app->db->createCommand('UPDATE object_flats SET price_usd = ROUND(price / :usd_bid) WHERE currency = :currency')
            ->bindValue(':usd_bid', $usd->bid)->bindValue(':currency', 'UAH')->execute();
        Yii::$app->db->createCommand('UPDATE object_flats SET price_usd = price WHERE currency = :currency')
            ->bindValue(':currency', 'USD')->execute();

//        $n = Yii::$app->db->createCommand("UPDATE privoz SET is_agent = 1 WHERE id = :id")
//            ->bindValue(':id', $aAllAds[$kR]['id'])
//            ->execute();

    }

}
