<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy
 * Date: 2015-12-04
 * Time: 18:06
 */

namespace app\models;
use Yii;
use yii\base\Model;

class ImportPhonesForm extends Model
{
    public $phones;

    public function rules()
    {
        return [
          [['phones'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phones' => 'Телефоны конкурентов'
        ];
    }

}