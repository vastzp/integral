<?php

namespace app\models;
use \yii\db\Expression;
use Yii;

/**
 * This is the model class for table "phones".
 *
 * @property integer $id
 * @property integer $phone
 * @property datetime $created
 * @property datetime $updated
 */
class Phones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone', 'created', 'updated',], 'string'],
            [['phone'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Номер телефона',
            'created' => 'Добавлен',
            'updated' => 'Обновлен',
        ];
    }
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
            $this->created = new Expression('NOW()');
        $this->updated = new Expression("NOW()");
        return parent::beforeSave($insert);
    }
}
