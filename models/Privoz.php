<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "privoz".
 *
 * @property integer $id
 * @property integer $vypusk
 * @property integer $year
 * @property string $action
 * @property string $razdel
 * @property string $text
 * @property string $phones
 * @property string $phones_jdac
 * @property string $phones_in_base
 * @property integer $count_phones_in_base // 0 - не проверяли, -1 - нет в базе, >= 1 в базе
 */
class Privoz extends \yii\db\ActiveRecord
{
    public function checkInBase()
    {
        $aPhones = explode(',', $this->phones_jdac);
        $count_phones_in_base = 0;
        $aPhones_in_base = [];
        foreach ($aPhones as $phone) {
//            echo "<pre>";
//            print_r($phone);
//            echo "</pre>";

            $aResult = yii::$app->db->createCommand("SELECT count(*) as cnt FROM object_flats WHERE phone like '%{$phone}%' ")->queryOne();
            $count_phones_in_base += $aResult['cnt'];
            $aPhones_in_base[$phone] = $aResult['cnt'];
        }

        $this->phones_in_base = serialize($aPhones_in_base);
        if ($count_phones_in_base == 0)
            $count_phones_in_base = -1;
        $this->count_phones_in_base = $count_phones_in_base;
        $this->save();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'privoz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vypusk', 'year', 'action', 'razdel', 'text', 'phones', 'phones_jdac'], 'required'],
            [['phones_in_base', 'count_phones_in_base'], 'safe'],
            [['vypusk', 'year', 'phones_max_count', 'is_agent'], 'integer'],
            [['action', 'razdel', 'text', 'phones', 'phones_jdac'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vypusk' => 'Выпуск',
            'year' => 'Год',
            'action' => 'Действие',
            'razdel' => 'Рубрика',
            'text' => 'Text',
            'phones' => 'Телефоны',
            'phones_jdac' => 'Телефоны JDAC',
            'phones_max_count' => 'Макс',
            'is_agent' => 'Агент',
            'phones_in_base' => 'Телефоны в базе',
            'count_phones_in_base' => 'В базе',

//            'id' => 'ID',
//            'vypusk' => 'Vypusk',
//            'year' => 'Year',
//            'action' => 'Action',
//            'razdel' => 'Razdel',
//            'text' => 'Text',
//            'phones' => 'Phones',
//            'phones_jdac' => 'Phones Jdac',
        ];
    }

    /**
     * Возвращает номер последнего выпуска привоза либо null
     * @return mixed - номер последнего выпуска привоза
     *
     * example:
     * Array
     * (
     * [vypusk] => 45
     * [year] => 2015
     * )
     */
    public static function getLastPrivoz()
    {
        $result = Yii::$app->db->createCommand("SELECT vypusk, year FROM privoz ORDER BY year DESC, vypusk DESC LIMIT 0, 1")->queryOne();

        if ($result) {
            list($aFinalResult['vypusk'], $aFinalResult['year']) = [$result['vypusk'], $result['year']];
            return $aFinalResult;
        } else
            return null;
    }

    /**
     * Проверяет есть ли объявления из привоза номер $nVypusk за $nYear год
     * @param $nYear
     * @param $nVypusk
     * @return возвращает количество объявлений в выпуске
     */
    public static function isVypuskInDB($nYear, $nVypusk)
    {
        $result = Yii::$app->db->createCommand("SELECT count(*) as cnt FROM privoz WHERE vypusk = :vypusk AND year = :year")
            ->bindValue(':vypusk', $nVypusk)
            ->bindValue(':year', $nYear)
            ->queryOne();
        return $result['cnt'];
    }

    public static function recoundAdsPronesCount($nYear, $nVypusk)
    {
        echo $nVypusk, $nYear;
        $aResult = Yii::$app->db->createCommand("SELECT id, year, vypusk, phones_jdac FROM privoz WHERE year = :year AND vypusk = :vypusk")
            ->bindValue(':year', $nYear)
            ->bindValue(':vypusk', $nVypusk)
            ->queryAll();

        foreach ($aResult as $k => $aAdsData) {

            $aPhones = [];
            if (strpos($aAdsData['phones_jdac'], ',') !== false) {
                $aPhones = explode(',', $aAdsData['phones_jdac']);
            } else {
                $aPhones[] = $aAdsData['phones_jdac'];
            }
            $aResult[$k]['phones'] = $aPhones;
        }
//        echo "<pre>";
//        print_r($aResult);
//        echo "</pre>";

        foreach ($aResult as $k => $v) {
            foreach ($v['phones'] as $kPhone => $vPhone) {
                $aPhonesCount[$vPhone][] = $k;
            }
        }

//        echo "<pre>";
//        print_r($aPhonesCount);
//        echo "</pre>";

        foreach ($aResult as $k => $v) {
            $aCounts = array();
            $sCountPhones = "";
            $aCountPhones = [];
            foreach ($v['phones'] as $kPhone => $vPhone) {
                $nCnt = count($aPhonesCount[$vPhone]);;
                $aCounts[$vPhone] = $nCnt;
                $aCountPhones[] = "$vPhone:$nCnt";
            }
            $sCountPhones = implode('|', $aCountPhones);
            $aResult[$k]['counts'] = $aCounts;
            $aResult[$k]['count_phones'] = $sCountPhones;
            $aResult[$k]['phones_max_count'] = max($aCounts);

            $n = Yii::$app->db->createCommand("UPDATE privoz SET count_phones = :count_phones, phones_max_count = :phones_max_count WHERE id = :id")
                ->bindValue(':id', $aResult[$k]['id'])
                ->bindValue(':count_phones', $aResult[$k]['count_phones'])
                ->bindValue(':phones_max_count', $aResult[$k]['phones_max_count'])
                ->execute();

        }


        echo "<pre>";
        //print_r($v);
        print_r($aResult);
        echo "</pre>";
        exit();


    }

    public static function setAgents($nYear, $nVypusk)
    {
        $x = 0;
        $nCountNewAgents = 0;

        echo $nVypusk, $nYear;
        $aAllAds = Yii::$app->db->createCommand("SELECT id, year, vypusk, phones_jdac, is_agent FROM privoz WHERE year = :year AND vypusk = :vypusk")
            ->bindValue(':year', $nYear)
            ->bindValue(':vypusk', $nVypusk)
            ->queryAll();

        $aAllAgents = Yii::$app->db->createCommand("SELECT id, phone FROM phones")->queryAll();

        foreach ($aAllAds as $kR => $vR) {
            $aPhones = [];
            if (strpos($vR['phones_jdac'], ',') !== false) {
                $aPhones = explode(',', $vR['phones_jdac']);
            } else
                $aPhones[] = $vR['phones_jdac'];

            //$aResult[$kR]['is_agent'] = 0;

            if ($vR['is_agent'] == 0) {
                echo ++$x . "<br />\n";
                foreach ($aPhones as $kP => $vP) {
                    foreach ($aAllAgents as $kA => $vA) {
                        if ($vP == $vA['phone']) {
                            $aAllAds[$kR]['is_agent'] = 1;
                            $nCountNewAgents++;
                            $n = Yii::$app->db->createCommand("UPDATE privoz SET is_agent = 1 WHERE id = :id")
                                ->bindValue(':id', $aAllAds[$kR]['id'])
                                ->execute();
                            echo " [agent]<br />";
                            break;
                        }

                    }
                }
            }
        }

        echo "<hr>Пометили " . $nCountNewAgents . " объявлений";
    }
}
