<?php

namespace app\models;

use \yii\db\Expression;

use Yii;

/**
 * This is the model class for table "object_flats".
 *
 * @property string $id
 * @property string $action
 * @property string $type - Тип недвижимости
 * @property string $rubrika
 * @property string $rooms - количество комнат
 * @property string $pref_street
 * @property string $street
 * @property string $address
 * @property string $house
 * @property string $flat_number
 * @property string $floor
 * @property string $floors
 * @property string $s1
 * @property string $s2
 * @property string $s3
 * @property string $text
 * @property string $notes
 * @property string $ad
 * @property string $phone
 * @property string $customer_name
 * @property string $price
 * @property string $currency
 * @property string $author
 * @property string $date_add
 * @property string $date_last_update
 * @property string $relevance
 */
class ObjectFlats extends \yii\db\ActiveRecord
{
    public static $fieldsValues = [
        'type' => [
            'Квартира' => 'Квартира',
            'Дом' => 'Дом',
            'Комната' => 'Комната',
            'Участок' => 'Участок',
            'Коммерческая недвижимость' => 'Коммерческая недвижимость',
            'Гаражи' => 'Гаражи',
            'Другое' => 'Другое',
        ],
        'action' => [
            'ПРОДАЮ' => 'ПРОДАЮ',
            'КУПЛЮ' => 'КУПЛЮ',
            'МЕНЯЮ' => 'МЕНЯЮ',
            'СДАЮ' => 'СДАЮ',
            'СНИМУ' => 'СНИМУ',
        ],
        'rubrika' => [
            //'-' => '- - - - -',
            'Жовтневый' => 'Жовтневый',
            'Шевченковский' => 'Шевченковский',
            'Орджоникидзевский' => 'Орджоникидзевский',
            'Заводский' => 'Заводский',
            'Ленинский' => 'Ленинский',
            'Коммунарский' => 'Коммунарский',
            'Хортицкий' => 'Хортицкий',
            'Другой населенный пункт' => 'Другой населенный пункт',
        ],
        'currency' => [
            '' => ' - ',
            'UAH' => 'грн.',
            'USD' => '$',
        ],
        'relevance' => [
            'Актуально' => 'Актуально',
            'Продано' => 'Продано',
            'Сдано' => 'Сдано',
            'Недозвон' => 'Недозвон',
            'Архив' => 'Архив',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object_flats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action',], 'required'],
            [
                ['text', 'notes'], 'string'
            ],
            [
                ['ad', 'price_uah', 'price_usd', 'customer_name'], 'safe'
            ],
            [
                [
                    'action', 'rubrika', 'rooms', 'pref_street', 'street', 'address', 'house', 'flat_number', 'floor',
                    'floors', 's1', 's2', 's3', 'phone', 'price', 'date_add', 'date_last_update', 'relevance',
                    'author', 'currency', 'type',
                ],
                'string', 'max' => 255
            ],
        ];
    }

	public function extraFields()
	{
		$fields = parent::fields();
		unset($fields['ad']);
		$fields['s'] = function () { return $this->s1 . '/'. $this->s2 . '/'. $this->s3; };
		return $fields;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Действие',
            'type' => 'Тип',
            'rubrika' => 'Район',
            'rooms' => 'Комнат',
            'pref_street' => 'Префикс',
            'street' => 'Улица',
            'address' => 'Адрес',
            'house' => 'Дом',
            'flat_number' => 'Кв',
            'floor' => 'Этаж',
            'floors' => 'Этажность',
            's1' => 'S общ',
            's2' => 'S жил',
            's3' => 'S кух',
            'text' => 'Текстовое описание',
            'notes' => 'Заметки',
            'ad' => 'Реклама',
            'phone' => 'Телефон',
            'customer_name' => 'Имя клиента',
            'price' => 'Цена',
            'currency' => 'Валюта',
            'price_uah' => 'UAH',
            'price_usd' => 'USD',
            'author' => 'Автор',
            'date_add' => 'Добавлено',
            'date_last_update' => 'Посл. обновление',
            'relevance' => 'Статус', // продано, сдано, актуально
        ];
    }

    public function beforeSave($insert)
    {
        $usd = Currency::findOne('USD');

        switch($this->currency)
        {
            case 'USD':
                $this->price_uah = round($this->price * $usd->ask);
                $this->price_usd = $this->price;
                break;

            case 'UAH':
                $this->price_usd = round($this->price / $usd->bid);
                $this->price_uah = $this->price;
                break;

        }

        if ($this->isNewRecord)
            $this->date_add = new Expression('NOW()');
        $this->date_last_update = new Expression("NOW()");

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttrivutes)
    {
        $phones = explode(',', $this->phone);
        foreach ($phones as $phone) {
            $aPrivoz = Privoz::find()->where("phones_jdac LIKE '%".$phone."%'")->all();
            foreach($aPrivoz as $privoz)
            {
                $privoz->checkInBase();
            }
        }

        parent::afterSave($insert, $changedAttrivutes);
    }

    /**
     * @return ReklamaToObject[]
     */
    public function getReklamaToObject()
    {
        $aReklama = Reklama::find()->indexBy('id')->all();
        $aReklamaToObject = [];
        foreach($aReklama as $kR => $vR)
        {
            $reklamaToObject = null;
            $reklamaToObject = ReklamaToObject::findOne(
                [
                    'id_object' => $this->id,
                    'id_reklama' => $kR,
                ]
            );

            if (is_null($reklamaToObject))
            {
                $reklamaToObject = new ReklamaToObject();
                $reklamaToObject->id_object = $this->id;
                $reklamaToObject->id_reklama = $kR;
                $reklamaToObject->save();
            }

            $aReklamaToObject[$kR] = $reklamaToObject;
        }
        return $aReklamaToObject;
    }
}
