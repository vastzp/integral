<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObjectFlats;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * ObjectFlatsSearch represents the model behind the search form about `app\models\ObjectFlats`.
 */
class ObjectFlatsSearch extends ObjectFlats
{
    public $price_usd_min;
    public $price_usd_max;
    public $price_uah_min;
    public $price_uah_max;
    public $updated_older_then;
    public $show_ads;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [
                [
                    'action', 'type', 'rubrika', 'rooms', 'street', 'address', 'house', 'flat_number', 'text', 'phone', 'price',
                    'date_add', 'date_last_update', 'relevance', 'ad',
                    'price_usd', 'price_usd_min', 'price_usd_max',
                    'price_uah', 'price_uah_min', 'price_uah_max',
                    'updated_older_then', 'show_ads',
                ],
                'safe'
            ],
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['show_ads'] = 'Реклама';

        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /**
         * @var ActiveQuery $query
         */
        $query = ObjectFlats::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'rubrika', $this->rubrika])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'rooms', $this->rooms])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'flat_number', $this->flat_number])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'date_add', $this->date_add])
            ->andFilterWhere(['like', 'date_last_update', $this->date_last_update])
            ->andFilterWhere(['like', 'relevance', $this->relevance])
            ->andFilterWhere(['>=', 'price_usd', $this->price_usd_min])
            ->andFilterWhere(['<=', 'price_usd', $this->price_usd_max])
            ->andFilterWhere(['>=', 'price_uah', $this->price_uah_min])
            ->andFilterWhere(['<=', 'price_uah', $this->price_uah_max])
            //->andFilterWhere('<', 'date_last_update', $this->updated_older_then)
            //->andFilterWhere('<', 'date_last_update', '2020-01-1')
            //->andWhere("date_last_update < '2016-01-01'", [])
            ;

        // Нет рекламы. Извращенный способ конечно
        if ($this->ad == '-1') { // нет рекламы
            $query->andFilterWhere(['=', 'length(ad)', '0']);
        }

        if ($this->ad == '1') { // есть реклама
            $query->andFilterWhere(['>', 'length(ad)', '0']);
        }


        return $dataProvider;
    }
}
