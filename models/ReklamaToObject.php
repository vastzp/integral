<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "reklama_to_object".
 *
 * @property string $id_object
 * @property string $id_reklama
 * @property string $value
 * @property string $created
 * @property string $updated
 */
class ReklamaToObject extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reklama_to_object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_object', 'id_reklama'], 'required'],
            [['id_object', 'id_reklama'], 'integer'],
            [['id_object', 'id_reklama', 'value', 'created', 'updated'], 'safe'],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_object' => 'Id Object',
            'id_reklama' => 'Id Reklama',
            'value' => 'Value',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }
}
