<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy
 * Date: 2015-11-30
 * Time: 16:59
 */

namespace app\models;


class PrivozParser
{
    public function __construct($sPrivozFile, $sPrivozNumber)
    {
        $aReserved = [
            'ПРОДАЮ',
            'КУПЛЮ',
            'МЕНЯЮ',
            'СДАЮ',
            'СНИМУ',
        ];
        echo "$sPrivozFile Parsing Privoz...";
        $aStrings = file($sPrivozFile);
        foreach ($aStrings as $k => $v) {
            $v = trim(iconv("cp1251", "UTF-8", $v));
            if ($v == '')
                continue;

            if (in_array($v, $aReserved))
                $sAction = $v;

            if (strpos($v, '#') === false) {
                $sRazdel = $v;
            } else {

                //echo "[" . $sAction . "](" . $sRazdel . ")" . $v;
                list ($sText, $sPhones) = explode('#', $v);
                $sJustDigitsAndComas = $this->justDigitsAndComas($sPhones);
                //echo "[[[" . $sJustDigitsAndComas . "]]]";
                //echo "<br />";

                $aData = array();
                $aData['vypusk'] = $sPrivozNumber;
                $aData['year'] = date("Y");

                $aData['action'] = $sAction;
                $aData['razdel'] = $sRazdel;
                $aData['text'] = $sText;
                $aData['phones'] = $sPhones;
                $aData['phones_jdac'] = $sJustDigitsAndComas;

                $aAllAds[] = $aData;

            }
        }

        \Yii::$app->db->createCommand()->batchInsert('privoz', array_keys($aData), $aAllAds)->execute();

        $this->result = 1;

    }

    public function justDigitsAndComas($sString)
    {
        $sResult = '';
        for ($i = 0; $i < strlen($sString); $i++) {
            $v = $sString[$i];
            if ((($v >= "0") && ($v <= "9")) || ($v == ','))
                $sResult .= $v;
        }
        return $sResult;
    }
}