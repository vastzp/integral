<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy
 * Date: 2015-11-30
 * Time: 16:37
 */
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadprivozForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $privozFile;
    public $finalFileName;
    public $privozNumber;

    public function rules()
    {
        return [
            [['privozFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->privozNumber = substr($this->privozFile->baseName, 1 + strpos($this->privozFile->baseName, '-'));
            $sFirstPartOfFileName = date('Y') . $this->privozNumber;
            $this->finalFileName = 'uploads/' . $sFirstPartOfFileName . '.' . $this->privozFile->extension;
            $this->privozFile->saveAs($this->finalFileName);
            return true;
        } else {
            return false;
        }
    }
}