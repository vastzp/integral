<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Privoz;

/**
 * PhonesSearch represents the model behind the search form about `app\models\Phones`.
 */
class PrivozSearch extends Privoz
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['vypusk', 'year', 'action', 'razdel', 'text', 'phones', 'phones_jdac'], 'required'],
            [['vypusk', 'year', 'phones_max_count', 'is_agent', 'count_phones_in_base',], 'integer'],
            [['action', 'razdel', 'text', 'phones', 'phones_jdac'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Privoz::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'vypusk' => $this->vypusk,
            'is_agent' => $this->is_agent,
            'count_phones_in_base' => $this->count_phones_in_base,
//            ['like', 'text', $this->text]
        ]);

        $query->andFilterWhere(
            ['like', 'text', $this->text]
        );
        $query->andFilterWhere(
            ['like', 'action', $this->action]
        );
        $query->andFilterWhere(
            ['like', 'razdel', $this->razdel]
        );
        $query->andFilterWhere(
            ['like', 'phones_jdac', $this->phones_jdac]
        );


        return $dataProvider;
    }
}
