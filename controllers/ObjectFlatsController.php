<?php

namespace app\controllers;

use app\models\Reklama;
use app\models\ReklamaToObject;
use Yii;
use app\models\ObjectFlats;
use app\models\ObjectFlatsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ObjectFlatsController implements the CRUD actions for ObjectFlats model.
 */
class ObjectFlatsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ObjectFlats models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ObjectFlatsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
//        $dataProvider->sort->attributes = [
//            'id' => [
//
//                'default' => SORT_DESC,
//            ],
//
//        ];

        // Рекламные площадки
        $aReklama = Yii::$app->db->createCommand("SELECT * FROM reklama ORDER BY sort")->queryAll();
        $aReklamaToObject = Yii::$app->db->createCommand("SELECT * FROM reklama_to_object")->queryAll();
        $aReklamaToObjectGood = [];
        foreach($aReklamaToObject as $kRTO => $vRTO)
        {
            $aReklamaToObjectGood[$vRTO['id_object']][$vRTO['id_reklama']] = $vRTO;
        }

//        echo "<pre>";
//        print_r($aReklama);
//        print_r($aReklamaToObject);
//        print_r($aReklamaToObjectGood);
//        echo "</pre>";
//        exit();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'aReklama' => $aReklama,
            'aReklamaToObject' => $aReklamaToObject,
            'aReklamaToObjectGood' => $aReklamaToObjectGood,

        ]);
    }

    /**
     * Displays a single ObjectFlats model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

       // $model = ObjectFlats::findOne($id);
//        $array = $model->toArray(['phone'], []);
//        echo "<pre>";
//        print_r($array);
//        echo "</pre>";
//        exit();


        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ObjectFlats model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ObjectFlats();
        if (isset($_REQUEST['ObjectFlats'])) {
            $model->attributes = $_REQUEST['ObjectFlats'];
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ObjectFlats model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->getReklamaToObject();



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // В этом цикле сохраняем рекламу
            foreach(Yii::$app->request->post()['ReklamaToObject'] as $kRTO => $vRTO)
            {
                $oRTM = ReklamaToObject::findOne($vRTO['id']);
                $oRTM->attributes = $vRTO;
                $oRTM->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
//                'aReklama' => $aReklama,
//                'aReklamaToObject' => $aReklamaToObject,
            ]);
        }
    }

    /**
     * Deletes an existing ObjectFlats model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ObjectFlats model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ObjectFlats the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ObjectFlats::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionNavigator()
    {
        $aActions = Yii::$app->db->createCommand("SELECT action FROM object_flats GROUP BY action")->queryAll();
        $aTypes = Yii::$app->db->createCommand("SELECT type FROM object_flats GROUP BY type")->queryAll();
        $aRubriks = Yii::$app->db->createCommand("SELECT rubrika FROM object_flats GROUP BY rubrika")->queryAll();
        return $this->render('navigator',
            [
                'aActions' => $aActions,
                'aTypes' => $aTypes,
                'aRubriks' => $aRubriks,
            ]
        );
    }
}
