<?php

namespace app\controllers;

use app\models\ImportPhonesForm;
use Yii;
use app\models\Phones;
use app\models\PhonesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PhonesController implements the CRUD actions for Phones model.
 */
class PhonesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Phones models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhonesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Phones model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Phones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Phones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Phones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Phones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Phones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Phones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Phones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport()
    {
        $model = new ImportPhonesForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $aStrings = explode("\n", $model->phones);
            foreach ($aStrings as $k => $v) {
                echo "<br />";
                $v = trim($v);

                echo $v . " ";
                if (strlen($v) != 10) {
                    echo "<span style='background-color: red;'>Номер телефона больше или меньше 10 символов. Добавьте его вручную!</span>";
                    continue;
                }


                // Проверяем есть ли такой конкурент в базе данных
                $aResult = Yii::$app->db->createCommand("SELECT count(*) as cnt FROM phones WHERE phone = :phone")->bindValue(':phone', $v)->queryOne();
                if ($aResult['cnt'] >= 1) {
                    echo "<span style='background-color: red;'>Такой номер телефона уже есть в Базе Данных</span>";
                    continue;
                } else {
                    // добавляем
                    Yii::$app->db->createCommand("INSERT INTO phones(phone) VALUES (:phone)")->bindValue(':phone', $v)->execute();
                    echo "<span style='background-color: green;'>Добавили</span>";
                }
            }


            exit();

        }

        return $this->render('import', ['model' => $model]);

    }

    public function actionAddagent($phone, $year, $vypusk)
    {
        $model = new Phones();
        $model->phone = $phone;
        $model->save();
        if ($model->id)
        {
            Yii::$app->db->createCommand("UPDATE privoz SET is_agent = 1 WHERE
              phones_jdac = '$phone'
              OR phones_jdac LIKE '%,$phone'
              OR phones_jdac LIKE '$phone,%'
              OR phones_jdac LIKE '%,$phone,%'
              ")->execute();
        }

        return $this->render('addagent', [
            'phone' => $phone,
            'year' => $year,
            'vypusk' => $vypusk,
            'model' => $model,
        ]);
    }
}
