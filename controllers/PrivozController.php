<?php

namespace app\controllers;

use app\models\PrivozParser;
use app\models\Privoz;
use app\models\PrivozSearch;
use Yii;
use yii\web\Controller;
use app\models\UploadprivozForm;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class PrivozController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new PrivozSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;

        foreach($dataProvider->getModels() as $model)
        {
            if ($model->count_phones_in_base == 0)
                $model->checkInBase();
        }

        $aLastPrivoz = Privoz::getLastPrivoz();

        // Кеширование Действий на 10 минут
        $aActions = Yii::$app->cache->get('actions');
        if ($aActions === false)
        {
            $aActions = ArrayHelper::map(Privoz::find('action', 'action')->distinct()->all(), 'action', 'action');
            Yii::$app->cache->set('actions', $aActions, 600);
        }

        // Кеширование Рубрик на 10 минут
        $aRubriks = Yii::$app->cache->get('rubriks');
        if ($aRubriks === false)
        {
            $aRubriks = ArrayHelper::map(Privoz::find('razdel', 'razdel')->distinct()->all(), 'razdel', 'razdel');
            Yii::$app->cache->set('rubriks', $aRubriks, 600);
        }

        return $this->render('index', [
            'aLastPrivoz' => $aLastPrivoz,  // данные о последнем выпуске привоза
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'aActions' => $aActions,
            'aRubriks' => $aRubriks,
        ]);

    }

    public function actionUploadfile()
    {


        $model = new UploadprivozForm();

        if (Yii::$app->request->isPost) {

            $model->privozFile = UploadedFile::getInstance($model, 'privozFile');

            if ($model->upload()) {
                // проверяем есть ли уже такой выпуск привоза

//                if ((Privoz::getLastPrivoz()['year'] == date("Y")) && (Privoz::getLastPrivoz()['vypusk'] == $model->privozNumber))
                if (Privoz::isVypuskInDB(date("Y"), $model->privozNumber)) {
                    $result = 2; // уже есть такой выпуск
                } else {

                    $oPrivozParser = new PrivozParser($model->finalFileName, $model->privozNumber);
                    $result = 1; // Новый выпуск загружен
                }
                return $this->render('upload', ['model' => $model, 'result' => $result]);
            }
        }

        return $this->render('upload', ['model' => $model]);
    }

    // Пересчитываем количество телефонов в текущем выпуске Привоза
    public function actionRecountphones()
    {
        $aLastVypusk = Privoz::getLastPrivoz();
        if ($aLastVypusk)
        {
            Privoz::recoundAdsPronesCount($aLastVypusk['year'], $aLastVypusk['vypusk']);
        }

        // последний выпуск привоза
        return $this->render('recountphones');
    }

    // помечаем конкурентов в текущем выпуске Привозад
    public function actionSetagetnts()
    {
        $aLastVypusk = Privoz::getLastPrivoz();
        if ($aLastVypusk)
        {
            Privoz::setAgents($aLastVypusk['year'], $aLastVypusk['vypusk']);
        }

        //return $this->render('recountphones');
    }

    public function actionLastvypusk()
    {
        return $this->redirect(['/privoz',
            'PrivozSearch[year]' => \app\models\Privoz::getLastPrivoz()['year'],
            'PrivozSearch[vypusk]' => \app\models\Privoz::getLastPrivoz()['vypusk'],
            'PrivozSearch[is_agent]' => 0,
        ]);
    }
}
