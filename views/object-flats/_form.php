<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectFlats */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="object-flats-form">

    <?php $form = ActiveForm::begin(); ?>

    <table width="100%">
        <tr>
            <td><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?></td>
        </tr>
    </table>

    <table>
        <tr>
            <td>
                <?= $form->field($model, 'action')->dropDownList(
                    $model::$fieldsValues['action']
                ) ?>
            </td>
            <td>&nbsp;</td>
            <td>
                <?= $form->field($model, 'type')->dropDownList(
                    $model::$fieldsValues['type']
                ) ?>
            </td>
            <td>&nbsp;</td>
            <td>
                <?= $form->field($model, 'rubrika')->dropDownList(
                    $model::$fieldsValues['rubrika']
                ) ?>
            </td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'pref_street')->dropDownList(['ул.' => 'ул.', 'пр.' => 'пр.', 'бул.' => 'бул.', 'пл.' => 'пл.', 'с.' => 'село', 'г.' => 'город', 'пгт' => 'пгт']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'house')->textInput(['maxlength' => true, 'size' => '1']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'flat_number')->textInput(['maxlength' => true, 'size' => '1']) ?></td>
        </tr>
    </table>


    <table>
        <tr>
            <td><?= $form->field($model, 'rooms')->textInput(['maxlength' => true, 'size' => '1']) ?></td>
            <td width="30px">&nbsp;</td>
            <td><?= $form->field($model, 'floor')->textInput(['maxlength' => true, 'size' => '1']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'floors')->textInput(['maxlength' => true, 'size' => '1']) ?></td>

            <td width="30px">&nbsp;</td>

            <td><?= $form->field($model, 's1')->textInput(['maxlength' => true, 'size' => '1']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 's2')->textInput(['maxlength' => true, 'size' => '1']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 's3')->textInput(['maxlength' => true, 'size' => '1']) ?></td>

            <td width="30px">&nbsp;</td>

            <td><?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'currency')->dropDownList($model::$fieldsValues['currency']) ?></td>
            <td width="30px">&nbsp;</td>
            <td><?= $form->field($model, 'relevance')->dropDownList($model::$fieldsValues['relevance']) ?></td>
            <td width="30px">&nbsp;</td>
            <td><?= $form->field($model, 'author')->dropDownList(['' => ' - - - - - ', 'Евгений' => 'Евгений', 'Ирина' => 'Ирина', 'Александ Иванович' => 'Александр Иванович']) ?></td>

        </tr>
    </table>


    <?php //echo $form->field($model, 'address')->textInput(['maxlength' => true]); ?>


    <table width="100%">
        <tr>
            <td><?= $form->field($model, 'text')->textarea(['rows' => 6]) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?></td>
        </tr>
    </table>


    <h4>Реклама</h4>
    <table>
        <?php
        if (isset($model->id)) {
            $i = 0;
            foreach ($model->getReklamaToObject() as $kRO => $vRO) {
                $i++;
                if (($i % 5) == 1)
                    echo "<tr>";
                echo "<td>";
                echo $this->render('_formRTO', [
                    'model' => $vRO,
                    'form' => $form,
                ]);
                echo "</td>";
                echo "<td>&nbsp;</td>";

                if (($i % 5) == 0) {
                    echo "</tr>";
                }
            }
        }
        ?>
    </table>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
