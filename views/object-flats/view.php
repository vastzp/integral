<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectFlats */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Object Flats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-flats-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $aPhones = [];
    $aPhones = explode(',', $model->phone);
    echo "<ul>";
    foreach ($aPhones as $kP => $vP) {
        echo "<li><strong>$vP</strong> <a href='http://kvartira.zp.ua/phones/addagent?phone=$vP&year=2016&vypusk=1' target='_blank'>[это агент]</a></li>";
    }
    echo "</ul>";

    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'action',
            'rubrika',
            'rooms',
            'street',
            //'address',
            'house',
            'flat_number',
            'text:ntext',
            'phone',
            'price',
            'date_add',
            'date_last_update',
            'relevance',
        ],
    ]) ?>

</div>
