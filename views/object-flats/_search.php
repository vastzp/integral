<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\ObjectFlats;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectFlatsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="object-flats-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <table>
        <tr>
            <td><?= $form->field($model, 'action')->dropDownList(array_merge(ObjectFlats::$fieldsValues['action'], ['' => '*Все'])) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'type')->dropDownList(array_merge(['' => '*Все'], ObjectFlats::$fieldsValues['type']), ['style' => 'width:120px;']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'rooms')->textInput(['style' => 'width:80px;']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'rubrika')->dropDownList(array_merge(['' => '*Все'], ObjectFlats::$fieldsValues['rubrika']), ['style' => 'width:180px;']) ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'street')->textInput(); ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'relevance')->dropDownList(array_merge(ObjectFlats::$fieldsValues['relevance'], ['' => '*Все'])) ?></td>
            <td>&nbsp;</td>
            <td>
                <div class="form-group">
                    <label class="control-label">Сортировка</label>
                    <select name="sort" class="form-control">
                        <option value="-date_last_update" selected>Обновлено &darr;</option>
                        <option value="date_last_update">Обновлено &uarr;</option>
                        <option value="-id">Id &darr;</option>
                        <option value="id">id &uarr;</option>
                    </select>
                </div>
            </td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'show_ads')->checkbox(); ?></td>
        </tr>
    </table>
    <!--table>
        <tr>
            <td><?= $form->field($model, 'price_usd_min')->label('USD от')->textInput(['style' => 'width:80px;']); ?></td>
            <td><?= $form->field($model, 'price_usd_max')->label('USD до')->textInput(['style' => 'width:80px;']); ?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'price_uah_min')->label('UAH от')->textInput(['style' => 'width:80px;']); ?></td>
            <td><?= $form->field($model, 'price_uah_max')->label('UAH до')->textInput(['style' => 'width:80px;']); ?></td>
        </tr>
    </table-->

    <?php // echo $form->field($model, 'rubrika') ?>

    <?php // echo $form->field($model, 'rooms') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'house') ?>

    <?php // echo $form->field($model, 'flat_number') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'date_add') ?>

    <?php // echo $form->field($model, 'date_last_update') ?>

    <?php // echo $form->field($model, 'relevance') ?>


    <div class="form-group">
        <?= Html::submitButton('Найти / применить', ['class' => 'btn btn-primary']) ?>
        <?php //echo Html::resetButton('Reset', ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
