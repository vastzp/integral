<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy
 * Date: 2016-02-10
 * Time: 17:52
 * @var \app\models\ReklamaToObject $model
 */
use app\models\Reklama;
use yii\helpers\Html;

$Reklama = Reklama::findOne($model->id_reklama);

?>


<?= Html::activeHiddenInput($model, "[{$model->id}]id") ?>

<?= Html::activeHiddenInput($model, "[{$model->id}]id_object") ?>

<?= Html::activeHiddenInput($model, "[{$model->id}]id_reklama") ?>



<?= $form->field($model, "[{$model->id}]value")->textInput(['maxlength' => true])->label(
    ($Reklama->ico != '')?Html::img($Reklama->ico, ['height' => 18]).' '.$Reklama->name:$Reklama->name
) ?>