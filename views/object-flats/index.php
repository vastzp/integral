<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObjectFlatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Объекты недвижимости';
$this->params['breadcrumbs'][] = $this->title;

$GLOBALS['searchModel'] = $searchModel;

$GLOBALS['aReklama'] = $aReklama;
$GLOBALS['aReklamaToObject'] = $aReklamaToObject;
$GLOBALS['aReklamaToObjectGood'] = $aReklamaToObjectGood;
?>
<p>
    <?= Html::a('Добавить новый объект недвижимости', ['create'], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
</p>

<div class="object-flats-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if (!isset($_REQUEST['ObjectFlatsSearch']))
        return;
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'action',
                'visible' => !$searchModel->action,
            ],
//            [
//                'attribute' => 'type',
//                'visible' => !$searchModel->type,
//            ],
            [
                'attribute' => 'rubrika',
                'visible' => !$searchModel->rubrika,
            ],
//            'rooms',
//            'street',
            //'address',
//            'house',
            //'flat_number',
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function ($data) {
                    /**
                     * @var app\models\ObjectFlats $data
                     */
                    $s = '';
                    $sType = '';
                    switch ($data->type) {
                        case 'Квартира':
                            $sType .= '<span style="background-color: #f2dede; color: black; border-radius: 5px; padding: 3px 5px 3px 5px;">квартира</span> ';
                            break;
                        /*
                        case 'Комната':
                            $sType .= '<span style="background-color: green; color: white; border-radius: 5px; padding: 3px 5px 3px 5px;">комната</span> ';
                            break;
                        */
                        default:
                            $sType .= '<span style="background-color: green; color: white; border-radius: 5px; padding: 3px 5px 3px 5px;">' . $data->type . '</span> ';

                    }
                    $sRooms = '';
                    $sRooms = '<span style="background-color: #afa; border-radius: 10px; width: 20px; height: 20px; padding: 3px 5px 3px 5px;">' . $data->rooms . '</span> комн. ';
                    $sStreet = '';
                    $sStreet .= '<span>' . $data->pref_street . ' ' . trim($data->street) . '</span>';
                    $sHouse = '';
                    if ($data->house)
                        $sHouse = '<span>, ' . $data->house . '</span> ';


                    $s .= '<div style="padding-bottom: 10px;">' . $sRooms . $sType . $sStreet . $sHouse . '</div>' . str_replace("\n", "<br />", $data->text) . "<div style=\"color: #888; font-size: 10px;\">Создано: " . $data->date_add . " Обновлено: " . Yii::$app->formatter->asRelativeTime($data->date_last_update) . "</div>";


                    return $s;
                }
            ],
//            'phone',
            //'price',
            [
                'attribute' => 'phone',
                //'value' => '))',
                'format' => 'raw',
                'value' => function ($data) {
                    return str_replace(',', '<br />', $data->phone);
                },
            ],
            [
                'attribute' => 'price_uah',
                //'value' => '))',
                'format' => 'raw',
                'value' => function ($data) {
                    $sClass = '';
                    if ($data->currency == 'UAH')
                        $sClass = 'bg-success';
                    return "<div class='" . $sClass . "' style='text-align: right;'><nobr>" . strrev(implode(' ', str_split(strrev($data->price_uah), 3))) . "</nobr></div>";
                },
            ],
            [
                'attribute' => 'price_usd',
                //'value' => '))',
                'format' => 'raw',
                'value' => function ($data) {
                    $sClass = '';
                    if ($data->currency == 'USD')
                        $sClass = 'bg-success';
                    return "<div class='" . $sClass . "' style='text-align: right;'><nobr>" . strrev(implode(' ', str_split(strrev($data->price_usd), 3))) . "</nobr></div>";
                },
            ],
            //'date_add',
            //'date_last_update',
            //'relevance',
            //'ad',
            [
                'format' => 'raw',
                'visible' => $GLOBALS['searchModel']->show_ads,
                'value' => function ($data) {
                    if ($GLOBALS['searchModel']->show_ads == 0)
                        return '0';

                    $sResult = '';
//                    $GLOBALS['aReklama'] = $aReklama;
//                    $GLOBALS['aReklamaToObject'] = $aReklamaToObject;
//                    $GLOBALS['aReklamaToObjectGood'] = $aReklamaToObjectGood;
                    /*
                     * Array
(
    [158] => Array
        (
            [4] => Array
                (
                    [id] => 12
                    [id_object] => 158
                    [id_reklama] => 4
                    [value] => http://zaporozhe.mesto.ua/sale/zhovtneviy-rajon/kvartal-malyij-ryinok/zaporozhskaya-ulitsa/5122850.html
                    [created] => 0000-00-00 00:00:00
                    [updated] => 0000-00-00 00:00:00
                )

            [3] => Array
                (
                    [id] => 11
                    [id_object] => 158
                    [id_reklama] => 3
                    [value] =>
                    [created] => 0000-00-00 00:00:00
                    [updated] => 0000-00-00 00:00:00
                )

            [5] => Array
                (
                    [id] => 34
                    [id_object] => 158
                    [id_reklama] => 5
                    [value] =>
                    [created] => 0000-00-00 00:00:00
                    [updated] => 0000-00-00 00:00:00
                )

        )

                     */
                    $i = 0;
                    foreach ($GLOBALS['aReklama'] as $kR => $vR) {
                        $background_color = '#eee';
                        $color = 'black';
                        $n = 0;
                        if (isset($GLOBALS['aReklamaToObjectGood'][$data->id][$vR['id']])) {
//                            echo "<pre>";
//                            print_r($GLOBALS['aReklamaToObjectGood'][$data->id]);exit();

                            $n = strlen($GLOBALS['aReklamaToObjectGood'][$data->id][$vR['id']]['value']);
                            if ($n > 0) {

                                $background_color = 'green';
                                $color = 'white';

                            }
                        }
                        $sResult .= '<span style="color: ' . $color . '; background-color: ' . $background_color . '; border-radius: 4px; margin: 3px; padding: 3px 3px 3px 3px;">' . $vR['short'] . '</span>';
                        if ((++$i % 3) == 0) $sResult .= "<div style='height: 10px;'></div>";
                    }

                    return $sResult;
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {info}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'update'), 'target' => '_blank',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>
