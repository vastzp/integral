<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrivozSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Privoz';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Работа с объявлениями Привоза</h1>
<?php if ($aLastPrivoz) { ?>
    <h2>Последний выпуск привоза №<?= $aLastPrivoz['vypusk'] ?> (<?= $aLastPrivoz['year'] ?>)</h2>
<?php } else { ?>
    <h2>Файл Привоза ещё не был загружен. Пожалуйста, сделайте это. </h2>
<?php } ?>

<a href="/privoz/uploadfile" class="btn btn-primary">Загрузить новый файл Привоза</a>
<a href="/privoz/recountphones" class="btn btn-primary">Пересчитать номера телефонов</a>
<a href="/privoz/setagetnts" class="btn btn-primary">Пометить объявления конкурентов</a>


<div class="privoz-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--p>
        <?= Html::a('Добавить объявление', ['create'], ['class' => 'btn btn-success']) ?>
    </p-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'year',
//            'vypusk',
//            'action',
            array(
                'attribute' => 'action',
                'value' => 'action',
                'filter' => $aActions,
            ),
            array(
                'attribute' => 'razdel',
                'value' => 'razdel',
                'filter' => $aRubriks,
            ),
            array(
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->text . '<br />' . Html::a(
                        '[в базу]',
                        [
                            '/object-flats/create',
                            'ObjectFlats[action]' => $data->action,
                            'ObjectFlats[rubrika]' => $data->razdel,
                            'ObjectFlats[text]' => $data->text,
                            'ObjectFlats[phone]' => $data->phones_jdac,
                        ],
                        ['target' => '_blank']
                    );
                },
            ),
            [
                'attribute' => 'phones_jdac',
                'format' => 'raw',
                'value' => function ($data) {
                    //$sResult = $data->phones."<hr>";
                    if (strpos($data->count_phones, '|')) {
                        $aPhonesData = explode('|', $data->count_phones);
                    } else {
                        $aPhonesData[] = $data->count_phones;
                    }
                    $sResult = '';
                    foreach ($aPhonesData as $k => $v) {
                        @list($nPhone, $nCount) = explode(':', $v);
                        $sResult .= '<nobr>' .
                            Html::a(
                                $nPhone,
                                ['/privoz/index',
                                    'PrivozSearch[year]' => $data->year,
                                    'PrivozSearch[vypusk]' => $data->vypusk,
                                    'PrivozSearch[phones_jdac]' => $nPhone,
                                ],
                                ['target' => '_blank'])
                            . ' (' . $nCount . ') <a href="/phones/addagent?phone=' . $nPhone . '&year=' . $data->year . '&vypusk=' . $data->vypusk . '" target="_blank">[агент]</a></nobr><br />'
//                            . Html::a(
//                                '[в базе?]',
//                                [
//                                    '/object-flats/index',
//                                    'ObjectFlatsSearch[phone]' => $data->phones_jdac,
//                                ],
//                                ['target' => '_blank']
//                            )
                            . "<br />";
                    }
                    return $sResult;
                },
            ],
            [
                'attribute' => 'phones_max_count'
            ],
            [
                'attribute' => 'phones_in_base',
                'format' => 'raw',
                'value' => function ($data) {
                    $aPhones_in_base = unserialize($data->phones_in_base);
                    $s = '';
                    foreach($aPhones_in_base as $phone => $count)
                    {
                        if ($count > 0)
                        {
                            $s .= Html::a(
                                '<span style="background-color: midnightblue; color: white; border-radius: 3px; padding: 3px 5px 3px 5px;">'.$phone.'</span> : '.$count,
                                [
                                    '/object-flats/index',
                                    'ObjectFlatsSearch[phone]' => $phone,
                                ],
                                ['target' => '_blank']
                            );
                        } else {
                            $s .= "$phone : $count<br />";
                        }
                        $s .= '<br />';
                    }
                    return $s;
                }
            ],
            'count_phones_in_base',
            'is_agent',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
