<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy
 * Date: 2015-11-30
 * Time: 16:47
 */

use yii\widgets\ActiveForm;

?>

<?php if (Yii::$app->request->isPost) { ?>

    <?php switch ($result) { ?>
<?php case '1': ?>
            <p class="bg-success">Файл Привоза успешно загружен</p>
            <?php
            break;
        case
            '2':?>
            <p class="bg-danger">Вы уже загружали этот выпуск Привоза</p>
            <?php
                break;
                ?>
        <?php } ?>

<?php } else { ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'privozFile')->fileInput() ?>

    <button>Submit</button>

    <?php ActiveForm::end() ?>
<?php } ?>
