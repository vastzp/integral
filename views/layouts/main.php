<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'АН "Интеграл" Запорожье',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    // Вот так делать во вьюхах нельзя :)
    $usd = \app\models\Currency::findOne('USD');

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
//            ['label' => 'Home', 'url' => ['/site/index']],
            [
                'label' => 'База',
                'url' => ['/object-flats']
            ],
            [
                'label' => 'Привоз',
                'url' => ['/privoz/lastvypusk',]
            ],


            ['label' => 'Конкуренты', 'url' => ['/phones/index?sort=-updated']],
            [
                'label' => 'USD ['.$usd['bid'].'] ['.$usd['ask'].']',
                'url' => ['/currency/get'],
                'options' => [
                    'style' => 'color: red',
                ]
            ],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; АН "Интеграл" Запорожье <?= date('Y') ?></p>

        <p class="pull-right">Программа работает <?php $datetime1 = new DateTime();
            $datetime2 = new DateTime('2015-12-07 15:00:00');
            $interval = $datetime1->diff($datetime2);
            $elapsed = $interval->format('%y лет %m месяцев %d дней %h часов %i минут %S секунд');
            echo $elapsed;
            ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
