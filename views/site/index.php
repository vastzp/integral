 <?php
use \yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Realta';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Realta</h1>

        <p class="lead">Управление базой данных АН "Интеграл" Запорожье.</p>

        <p><a class="btn btn-lg btn-success" href="/privoz/lastvypusk">Выпуск Привоза №<?=\app\models\Privoz::getLastPrivoz()['vypusk']?> <?=\app\models\Privoz::getLastPrivoz()['year']?></a></p>
        <p><?php  ?></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>База объектов недвижимости</h2>

                <p>Теперь работать с базой объектов недвижимости стало удобнее.</p>
                <ul>
                    <li>Работа с объектами недвижимости:
                        <ul>
                            <li>добавление;</li>
                            <li>редактирование;</li>
                            <li>удаление.</li>
                        </ul>
                    </li>
                    <li>Фильтрация объектов по:
                        <ul>
                            <li>действию;</li>
                            <li>типу недвижимости;</li>
                            <li>району;</li>
                            <li>наличию рекламы в OLX, собственном сайте, привозе и т.д.;</li>
                            <li>текстовому описанию;</li>
                        </ul>
                    </li>
                    <li>Автоматический пересчет стоимости в соответствии с текущими курсами валют</li>
                </ul>
                <p><?= Html::a('База объектов недвижимости &raquo;', \yii\helpers\Url::to('object-flats'), ['class' => 'btn btn-default'])?></p>
            </div>
            <div class="col-lg-4">
                <h2>Привоз</h2>

                <p>Прозванивайте выпуски Привоза эффективно:</p>
                <ul>
                    <li>автоматическая фильтрация объявлений конкурентов;</li>
                    <li>автоматизированное добавление объявления в свою базу объектов недвижимости, с помощью одного клика;</li>
                    <li>добавление номеров телефонов конкурентов в один клик;</li>
                </ul>

                <p><?= Html::a('Последний выпуск Привоза &raquo;', \yii\helpers\Url::to('privoz/lastvypusk'), ['class' => 'btn btn-default'])?></p>
            </div>
            <div class="col-lg-4">
            <div class="col-lg-4">
                <h2>Курсы валют</h2>

                <p>Обновление курса валют </p>

            </div>
        </div>

    </div>
</div>
