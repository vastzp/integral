<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhonesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Phones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phones-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Phones', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Массовове добавление номеров конкурентов', ['import'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'phone',
            [
                'label'=>'Добавлен',
                'format'=>'raw',
                'value' => function($data)
                {
                    return Yii::$app->formatter->asRelativeTime((new DateTime($data->created))->getTimestamp());
                }
            ],
            [
                'label'=>'Обновлен',
                'format'=>'raw',
                'value' => function($data)
                {
                    return Yii::$app->formatter->asRelativeTime((new DateTime($data->updated))->getTimestamp());
                }
            ],
            'created',
            'updated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
