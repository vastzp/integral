<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy
 * Date: 2015-12-04
 * Time: 18:11
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Массовое добавление телефонов конкурентов</h1>
<p>
    При добавлении конкурентов вводите номера телефонов без дополнительных символов.
    <br>Только цифры!
    <br>Каждый номер телефона с новой строки.
    <br>
</p>
<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'phones')->textarea(['rows' => 10]) ?>
<div class="form-group">
        <?= Html::submitButton('Добавить конкурентов массово', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
